export const TYPE_GROUPS_SAVED_REPLY = 'Groups::SavedReply';
export const TYPENAME_AI_AGENT = 'Ai::Agent';
export const TYPENAME_AI_AGENT_VERSION = 'Ai::AgentVersion';
